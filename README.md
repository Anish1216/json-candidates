# README #

This repo demonstrates the use of the external library JSON Simple in reading and parsing a JSON document.

Here, we pull from a document with a list of US presidential candidates for the 2016 election cycle. Chances are, by the time you read this, this project will be outdated. Still, it should serve as a handy guide for implementing JSON in your various Java projects.

Note the implicit use of iteration here, and that the library (like other JSON libraries for Java) returns Objects, which have to be typecast into various JSON types.


# Links #

JSON Simple: https://code.google.com/archive/p/json-simple/