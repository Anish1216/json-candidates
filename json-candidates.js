{
	"candidates": [
		{
			"firstName": "Donald"
			"lastName": "Trump"
			"party": "Republican"
			"occupation": "CEO"
		}
		
		{
			"firstName": "Ted"
			"lastName": "Cruz"
			"party": "Republican"
			"occupation": "Senator"
		}
		
		{
			"firstName": "John"
			"lastName": "Kasich"
			"party": "Republican"
			"occupation": "Governor"
		}
		
		{
			"firstName": "Hillary"
			"lastName": "Clinton"
			"party": "Democrat"
			"occupation": "Secretary of State"
		}
		
		{
			"firstName": "Bernie"
			"lastName": "Sanders"
			"party": "Democrat"
			"occupation": "Senator"
		}
		
		{
			"firstName": "Gary"
			"lastName": "Johnson"
			"party": "Libertarian"
			"occupation": "Governor"
		}
		
		{
			"firstName": "Austin"
			"lastName": "Petersen"
			"party": "Libertarian"
			"occupation": "Troll"
		}
		
		{
			"firstName": "John"
			"lastName": "McAfee"
			"party": "Libertarian"
			"occupation": "CEO"
		}
	]
}