package jsonwork;

import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

import org.json.simple.JSONArray;
import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;

/**
 * 
 * @author AnishKrishnan
 * 
 * This program loads a JSON file of 2016 POTUS candidates
 * and creates objects with their information.
 *
 */

class Candidate {
	
	private String name, party, occupation;
	
	public Candidate(String nameIn, String partyIn, String occupationIn) {
		name = nameIn;
		party = partyIn;
		occupation = occupationIn;
	}
	
	public String getName() {
		return name;
	}
	
	public String getParty() {
		return party;
	}
	
	public String getOccupation() {
		return occupation;
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("---- Presidential Candidate ----");
		sb.append("\nName: " + name);
		sb.append("\nParty: " + party);
		sb.append("\nOccupation: " + occupation);
		sb.append("\n");
		
		return sb.toString();
	}
}

public class CandidateLoader {

	public static void main(String[] args) {
		JSONParser jsp = new JSONParser();

		try {
			FileReader fr = new FileReader("C:\\ANISH\\Java\\Workspace\\TheBasics\\json-candidates.js");

			/* Note that JSON Object methods return Objects. */
			JSONObject jso = (JSONObject) jsp.parse(fr);
			JSONArray candidates = (JSONArray) jso.get("candidates");
			
			ArrayList<Candidate> candidateList = new ArrayList<Candidate>();
			
			for (Object j : candidates.toArray()) {
				JSONObject jObj = (JSONObject) j;	// Cast to JSONObject.
				
				/* Pull values from the JSON document. */
				String firstName = jObj.get("firstName").toString();
				String lastName = jObj.get("lastName").toString();
				String name = firstName + " " + lastName;
				String party = jObj.get("party").toString();
				String occupation = jObj.get("occupation").toString();
				
				candidateList.add(new Candidate(name, party, occupation));
			}
			
			for (Candidate c : candidateList)
				System.out.println(c.toString());

			/* You'll need the IOException for FileReader
			 * and the ParseException for JSONParser. */
		} catch (IOException | ParseException e) {
			e.printStackTrace();
		}
	}
}
